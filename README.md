# Angular Pokemon Trainer

# Description
The first page you go to is the landing page. After logging in you'll be redirected to the pokemon catalogue page. Here you can catch pokemon. If you've already caught the pokemon and try to press the button it will not be duplicated to the list. In the navbar you can redirect to the trainer page, where the caught pokemon are displayed with name and image. Every pokemon has an option to be released with a button. When the button is pressed the selected pokemon is removed from the list and the pokemon is now available to be caught again in the pokemon catalogue

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.1.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Heroku
https://am-noroff-api.herokuapp.com/

## Bugs
I had som difficulities with the basic "ash" username because his two pokemon are not pokemon objects. I tried to buy pass that so the code just ignores those two, but could not find a way. So when ash is the logged in user, the pokemon are not displayed in the trainer page. 
There is a bug somewhere in the code. Whenever a user release a pokemon the pokemon disappears, but if you do not catch a new pokemon after this and then reload the page, the "original" list is displayed again

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
