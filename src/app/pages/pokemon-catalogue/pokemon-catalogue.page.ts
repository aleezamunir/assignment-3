import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';

@Component({
  selector: 'app-pagespokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css'],
})
export class PokemonCatalogue implements OnInit {
  public get pokemon(): Pokemon[] {
    return this.pokemonCatalogueService.pokemon
  }

  public get errors(): string {
    return this.pokemonCatalogueService.error
  }
  constructor(
    private readonly pokemonCatalogueService: PokemonCatalogueService
  ) {}

  ngOnInit(): void {
    this.pokemonCatalogueService.findAllPokemon()
  }
}
