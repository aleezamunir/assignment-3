import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pageslogin',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.css'],
})
export class Landing {
  constructor(private readonly router: Router) {}

  //Goes to pokemon catalogue when the user is logged in
  handleLogin(): void {
    this.router.navigateByUrl('/pokemon')
  }
}
