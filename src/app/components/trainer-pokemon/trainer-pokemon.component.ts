import { Component, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer-pokemon',
  templateUrl: './trainer-pokemon.component.html',
  styleUrls: ['./trainer-pokemon.component.css'],
})
export class TrainerPokemonComponent implements OnInit {
  constructor(private trainerService: TrainerService) {}

  trainer: Trainer | undefined
  ngOnInit(): void {
    this.trainer = this.trainerService.trainer
  }
}
