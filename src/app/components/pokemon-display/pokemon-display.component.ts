import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-pokemon-display',
  templateUrl: './pokemon-display.component.html',
  styleUrls: ['./pokemon-display.component.css'],
})
export class PokemonDisplayComponent implements OnInit {
  public isReleased: Boolean = false;
  @Input()
  pokemon: Pokemon[] | undefined = [];

  pokemonName: string = '';

  constructor() {}

  ngOnInit(): void {}
}
