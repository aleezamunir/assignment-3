import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CatchService } from 'src/app/services/catch.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-release-button',
  templateUrl: './release-button.component.html',
  styleUrls: ['./release-button.component.css'],
})
export class ReleaseButtonComponent implements OnInit {
  @Input() pokemonName: string = ''
  isReleased: Boolean = false
  constructor(
    private trainerService: TrainerService,
    private catchService: CatchService
  ) {}

  ngOnInit(): void {}
  //Releases the pokemon from the trainers array and changes the isReleased to true
  public onReleaseClick() {
    this.catchService.releasePokemon(this.pokemonName).subscribe({
      next: (response: Trainer) => {
        this.isReleased = this.trainerService.inCaughtPokemon(this.pokemonName)
      },
      error: (error: HttpErrorResponse) => {
        console.log('Error: ' + error.message)
      },
    })
  }
}
