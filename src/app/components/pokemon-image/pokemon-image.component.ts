import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';

@Component({
  selector: 'app-pokemon-image',
  templateUrl: './pokemon-image.component.html',
  styleUrls: ['./pokemon-image.component.css'],
})
export class PokemonImageComponent implements OnInit {
  @Input()
  pokemon!: Pokemon;
  constructor(private catalogueService: PokemonCatalogueService) {}

  ngOnInit(): void {
    this.pokemon.image = this.catalogueService.getImage(this.pokemon.url)
  }
}
