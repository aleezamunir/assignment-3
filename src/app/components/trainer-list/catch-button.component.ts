import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { Trainer } from 'src/app/models/trainer.model';
import { CatchService } from 'src/app/services/catch.service';
import { TrainerService } from 'src/app/services/trainer.service';
import { StorageUtil } from 'src/app/utils/storage.util';

@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.css'],
})
export class CatchButton implements OnInit {
  public isCaught: boolean = false;
  @Input() pokemonName: string = ''

  get isCaughtPokemon(): boolean {
    return this.trainerService.inCaughtPokemon(this.pokemonName)
  }

  constructor(
    private trainerService: TrainerService,
    private readonly catchService: CatchService
  ) {}

  ngOnInit(): void {
    this.isCaught = this.trainerService.inCaughtPokemon(this.pokemonName)
  }

  //Catches the pokemon to the trainers array and changes the isCaught satus to true
  onCatchClick(): void {
    this.catchService.addToPokemon(this.pokemonName).subscribe({
      next: (response: Trainer) => {
        this.isCaught = this.trainerService.inCaughtPokemon(this.pokemonName)
        console.log(this.isCaught)
      },
      error: (error: HttpErrorResponse) => {
        console.log('Error: ' + error.message)
      },
    })
  }
}
