export class StorageUtil {
  //Saves to sessionstorage
  public static storageSave<T>(key: string, value: T): void {
    sessionStorage.setItem(key, JSON.stringify(value));
  }
  //Reades from sessionsstorage
  public static storageRead<T>(key: string): T | undefined {
    const storedValue = sessionStorage.getItem(key);
    try {
      if (storedValue) {
        return JSON.parse(storedValue) as T;
      } else {
        return undefined;
      }
    } catch (e) {
      sessionStorage.removeItem(key);
      return undefined;
    }
  }
}
