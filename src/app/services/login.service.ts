import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';

const { apiTrainers, apiKey } = environment;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private readonly http: HttpClient) {}

  public login(username: string): Observable<Trainer> {
    return this.checkUsername(username).pipe(
      switchMap((response: Trainer | undefined) => {
        if (response === undefined) {
          return this.createUser(username)
        }
        return of(response)
      })
    )
  }
  //Checks if a user exists
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${apiTrainers}?username=${username}`).pipe(
      map((response: Trainer[]) => {
        return response.pop()
      })
    )
  }
  //Creates a new user with an empty pokemon array
  private createUser(username: string): Observable<Trainer> {
    const trainer = {
      username,
      pokemon: [],
    }

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': apiKey,
    })

    return this.http.post<Trainer>(apiTrainers, trainer, {
      headers,
    })
  }
}
