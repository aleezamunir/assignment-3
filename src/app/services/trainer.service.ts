import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  private _trainer?: Trainer

  public get trainer(): Trainer | undefined {
    return this._trainer
  }

  public set trainer(trainer: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer
  }
  constructor() {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
  }
  //Checks to see if a pokemon is already caught by the user.
  public inCaughtPokemon(name: string): boolean {
    if (this._trainer) {
      return Boolean(
        this._trainer.pokemon.find((pokemon: Pokemon) => pokemon.name === name)
      )
    }
    return false
  }
}
