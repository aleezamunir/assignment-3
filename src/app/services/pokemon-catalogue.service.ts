import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Pokemon, PokemonResponse } from '../models/pokemon.model';
const { apiPokemon } = environment;

@Injectable({
  providedIn: 'root',
})
export class PokemonCatalogueService {
  public _pokemon: Pokemon[] = []
  public _error: string = ''

  public get pokemon(): Pokemon[] {
    return this._pokemon
  }

  public get error(): string {
    return this._error
  }

  constructor(private readonly http: HttpClient) {}
  //Finds the pokemon from the pokemon API. The Api is limited to 20 pokemon from 1 query
  public findAllPokemon(): void {
    this.http.get<PokemonResponse>(apiPokemon).subscribe({
      next: (response: PokemonResponse) => {
        this._pokemon = response.results
      },
      error: (error: HttpErrorResponse) => {
        this._error = error.message
      },
    })
  }
  //Gets the image of the pokemon using the url
  public getImage(url: string): string {
    const id = url.split('/').filter(Boolean).pop();
    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`
  }
  //Finds the selected pokemon
  public pokemonByName(name: string): Pokemon | undefined {
    return this._pokemon.find((pokemon: Pokemon) => pokemon.name === name)
  }
}
