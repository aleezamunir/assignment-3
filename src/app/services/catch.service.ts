import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { TrainerService } from './trainer.service';
import { Trainer } from '../models/trainer.model';
import { Observable, tap } from 'rxjs';

const { apiKey, apiTrainers } = environment;

@Injectable({
  providedIn: 'root',
})
export class CatchService {
  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonCatalogueService,
    private readonly trainerService: TrainerService
  ) {}

  public addToPokemon(pokemonName: string): Observable<Trainer> {
    if (!this.trainerService.trainer) {
      throw new Error('catchToList: There is no trainer')
    }

    const trainer: Trainer = this.trainerService.trainer

    const pokemon: Pokemon | undefined =
      this.pokemonService.pokemonByName(pokemonName)
    console.log(pokemon);
    if (!pokemon) {
      throw new Error('No pokemon with this name: ' + pokemonName)
    }

    if (this.trainerService.inCaughtPokemon(pokemonName)) {
      throw new Error('Pokemon is already caught')
    }

    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      'x-api-key': apiKey,
    })

    return this.http
      .patch<Trainer>(
        `${apiTrainers}/${trainer.id}`,
        {
          pokemon: [...trainer.pokemon, pokemon],
        },
        {
          headers,
        }
      )
      .pipe(
        tap((updatedTrainer: Trainer) => {
          this.trainerService.trainer = updatedTrainer;
        })
      )
  }

  public releasePokemon(pokemonName: string): Observable<Trainer> {
    if (!this.trainerService.trainer) {
      throw new Error('there is no trainer')
    }

    this.trainerService.trainer.pokemon =
      this.trainerService.trainer.pokemon.filter(
        (pokemon: Pokemon) => pokemon.name !== pokemonName
      )
    console.log(this.trainerService.trainer.pokemon);

    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      'x-api-key': apiKey,
    })

    return this.http
      .patch<Trainer>(
        `${apiTrainers}/${this.trainerService.trainer.id}`,
        {
          pokemon: [this.trainerService.trainer.pokemon],
        },
        {
          headers,
        }
      )
      .pipe(
        tap(
          (updatedTrainer: Trainer) =>
            this.trainerService.trainer === updatedTrainer
        )
      )
  }
}
