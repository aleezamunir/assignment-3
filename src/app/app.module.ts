import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { Landing } from './pages/landing/landing.page';
import { PokemonCatalogue } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { FormsModule } from '@angular/forms';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { CatchButton } from './components/trainer-list/catch-button.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PokemonImageComponent } from './components/pokemon-image/pokemon-image.component';
import { TrainerPokemonComponent } from './components/trainer-pokemon/trainer-pokemon.component';
import { PokemonDisplayComponent } from './components/pokemon-display/pokemon-display.component';
import { ReleaseButtonComponent } from './components/release-button/release-button.component';

@NgModule({
  //components
  declarations: [
    AppComponent,
    Landing,
    PokemonCatalogue,
    TrainerPage,
    LoginFormComponent,
    PokemonListComponent,
    CatchButton,
    NavbarComponent,
    PokemonImageComponent,
    TrainerPokemonComponent,
    PokemonDisplayComponent,
    ReleaseButtonComponent,
  ],
  //Modules
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
